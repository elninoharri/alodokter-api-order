# Lumen API Alodokter Order

API with Lumen 8.

# Lumen PHP Framework

[![Build Status](https://travis-ci.org/laravel/lumen-framework.svg)](https://travis-ci.org/laravel/lumen-framework)
[![Total Downloads](https://img.shields.io/packagist/dt/laravel/framework)](https://packagist.org/packages/laravel/lumen-framework)
[![Latest Stable Version](https://img.shields.io/packagist/v/laravel/framework)](https://packagist.org/packages/laravel/lumen-framework)
[![License](https://img.shields.io/packagist/l/laravel/framework)](https://packagist.org/packages/laravel/lumen-framework)

Laravel Lumen is a stunningly fast PHP micro-framework for building web applications with expressive, elegant syntax. We believe development must be an enjoyable, creative experience to be truly fulfilling. Lumen attempts to take the pain out of development by easing common tasks used in the majority of web projects, such as routing, database abstraction, queueing, and caching.

## Official Documentation

Documentation for the framework can be found on the [Lumen website](https://lumen.laravel.com/docs).

### Installation Using Docker

- Clone the Repo:
    - `git clone git@gitlab.com:elninoharri/alodokter-api-product.git`
    - `git clone https://gitlab.com/elninoharri/alodokter-api-product.git`
- `cd alodokter-api-product`
- SSH into the Docker container with `make ssh` and run the following.
    - `composer create-project`
    - `php artisan migrate`
- Exit from Docker container with `CTRL+C` or `exit`.
- Rename `docker-compose.local.yaml` to `docker-compose.overridee.yaml`
- Start the local development server with `make up`.
- Run tests with `make dev-test`.
- Run `make` to see available commands.
- Always `ssh` into Docker container `app` by running `make ssh` before executing any `artisan` commands.

### Installation On Local
- Clone the Repo:
    - `git clone git@gitlab.com:elninoharri/alodokter-api-product.git`
    - `git clone https://gitlab.com/elninoharri/alodokter-api-product.git`
- `cd alodokter-api-product`
- On Visual Studio Code Terminal run the following.
    - `composer install`
    - `php artisan migrate`
    - `php -S localhost:8800 -t public`


