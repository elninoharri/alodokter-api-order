<?php

namespace App\Alodokter;

use GuzzleHttp\Client;
use GuzzleHttp\Exception\RequestException;
use Illuminate\Support\Facades\Log;

class Product
{
    // private $client;

    // public function __construct($language='id')
    // {
    //     $this->client = new Client([
    //         // Base URI is used with relative requests
    //         'base_uri' => env('ALODOKTER_API_PRODUCT_URL'),
    //         // You can set any number of default request options.
    //         'timeout' => 60.0,
    //         'headers' => [
    //             'Content-Type' => 'application/json',
    //             'Accept-Language' => $language,
    //             'x-api-key' => env('ALODOKTER_API_PRODUCT_KEY'),
    //         ]
    //     ]);
    // }

    public function getProductDetail($product_id)
    {
        $url = env('ALODOKTER_API_PRODUCT_URL') . 'api/v1/detail/' . $product_id;
        $headers = [
            'Content-Type' => 'application/json',
            'Accept-Language' => 'id',
            'x-api-key' => env('ALODOKTER_API_PRODUCT_KEY'),
        ];
        
        $ch = curl_init();
            
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        
        // Execute post
        $result = curl_exec($ch);
        $jsonResult = json_decode($result);

        return $jsonResult;

        curl_close($ch);

        // $response = $this->client->request('GET', 'api/v1/detail/');

    }

    // public function request($method = 'GET', $endpoint = '/', $options = [])
    // {
    //     try {
    //         $response = $this->client->request($method, $endpoint, $options);
    //     } catch (RequestException $e) {
    //         Log::error($e->getCode() . ' ' . $e->getMessage());
    //         $exception = [
    //             'message' => trim(strip_tags($e->getMessage())),
    //             'code' => $e->getCode(),
    //             'error' => true,
    //             'endpoint' => '[' . $method . ']' . $endpoint
    //         ];

    //         if ($e->hasResponse()) {
    //             $bodyResponse = $e->getResponse()->getBody();
    //             $exception = array_merge($exception, json_decode($bodyResponse, true) ?? []);
    //         }

    //         return (object)$exception;
    //     }

    //     if ($response->getStatusCode() >= 200) {
    //         return $this->responseValid(json_decode($response->getBody()));
    //     } else {
    //         return null;
    //     }
    // }

    // private function responseValid($data)
    // {
    //     if ($data) {
    //         if (property_exists($data, 'status')) {
    //             if (isset($data->status->code) && $data->status->code === 1) {
    //                 return $data->data;
    //             }

    //             if ($data->status == '1') {
    //                 return $data;
    //             }
    //         }

    //         if (property_exists($data, 'code')) {
    //             if ($data->code === '1') {
    //                 return $data;
    //             }
    //         }
    //     }

    //     return null;
    // }

}