<?php

namespace App\Http\Controllers;

use Validator;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use App\Models\User;
use Symfony\Component\HttpFoundation\Cookie;
use Symfony\Component\HttpFoundation\Response;

class UserController extends Controller
{

    public function register(Request $request)
    {
        $validator = Validator::make($request->all(), [
            "email" => "required|unique:users|email",
            "password" => "required"
        ]);

        if ($validator->fails()) {
            $errorString = implode(",",$validator->messages()->all());
            return response()->json([
                'status' => env('STATUS_FAILED_TEXT'),
                'message' => $errorString
            ], 400);
        }

        $email = $request->input('email');
        $password = Hash::make($request->input('password'));
        
        try {
            $user = User::create([
                'email' => $email,
                'password' => $password
            ]);
    
            return response()->json([
                'status' => env('STATUS_SUCCESS_TEXT'),
                'message' => 'Successfully registration'
            ], 201);

        } catch (\Throwable $th) {
            return response()->json([
                'status' => env('STATUS_FAILED_TEXT'),
                'message' => 'Error while registration'
            ], 400);
        }
    }

    public function login(Request $request)
    {
        $validator = Validator::make($request->all(), [
            "email" => "required|email",
            "password" => "required|min:6"
        ]);

        if ($validator->fails()) {
            $errorString = implode(",",$validator->messages()->all());
            return response()->json([
                'status' => env('STATUS_FAILED_TEXT'),
                'message' => $errorString
            ], 400);
        }

        $email = $request->input('email');
        $password = $request->input('password');
  
        $user = User::where('email', $email)->first();
        
        if (!$user) {
            return response()->json([
                'status' => env('STATUS_FAILED_TEXT'),
                'message' => 'Login failed, invalid username or password ...'
            ], 401);
        }
  
        $isValidPassword = Hash::check($password, $user->password);
        if (!$isValidPassword) {
            return response()->json([
                'status' => env('STATUS_FAILED_TEXT'),
                'message' => 'Login failed, invalid username or password ...'
            ], 401);
        }
  
        $generateToken = bin2hex(random_bytes(40));
        
        try {
            $user->update([
                'token' => $generateToken
            ]);

            return response()->json([
                'status' => env('STATUS_SUCCESS_TEXT'),
                'data' => ([
                    'user' => $user
                ])
            ], 200);

        } catch (\Throwable $th) {
            return response()->json([
                'status' => env('STATUS_FAILED_TEXT'),
                'message' => 'Error While Login'
            ], 401);
        }
    }
} 