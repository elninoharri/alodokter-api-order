<?php

namespace App\Http\Controllers;

use Validator;
use Illuminate\Http\Request;
use App\Models\UserCart;
use App\Alodokter\Product;

class CartController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        try {
            $userCart = UserCart::all();
        
            return response()->json([
                'status' => env('STATUS_SUCCESS_TEXT'),
                'data' => ([
                    'cart' => $userCart
                ])
            ], 200);

        } catch (\Throwable $th) {
            return response()->json([
                'status' => env('STATUS_FAILED_TEXT'),
                'message' => 'Error While Fetching All Data'
            ], 400);
        }
    }

    public function create(Request $request)
    {
        # code...
    }
    
    public function cart(Request $request)
    {
        $data = $request->all();
        
        $validator = Validator::make($request->all(), [
            "user_id" => "required",
            "product_id" => "required"
        ]);
        
        if ($validator->fails()) {
            $errorString = implode(",",$validator->messages()->all());
            return response()->json([
                'status' => env('STATUS_FAILED_TEXT'),
                'message' => $errorString
            ], 400);
        }
        
        try {

            $data['qty'] = 1;
            $userCart = UserCart::create($data);

            if ($userCart) {
                return response()->json([
                    'status' => env('STATUS_SUCCESS_TEXT'),
                    'message' => 'Successfully add a product to cart'
                ], 200);
            }

        } catch (\Throwable $th) {
            return response()->json([
                'status' => env('STATUS_FAILED_TEXT'),
                'message' => 'Error While Create Product'
            ], 400);
        }   
    }

    public function checkout(Request $request)
    {
        $data = $request->all();
        
        $userCart = UserCart::where('user_id', $data['user_id'])
        ->where('status', null)
        ->orderBy('updated_at', 'DESC')
        ->first();

        if (!$userCart) {
            return response()->json([
                'status' => env('STATUS_FAILED_TEXT'),
                'message' => 'Cart not found'
            ], 404);
        }

        $validator = Validator::make($request->all(), [
            "user_id" => "required"
        ]);

        if ($validator->fails()) {
            $errorString = implode(",",$validator->messages()->all());
            return response()->json([
                'status' => env('STATUS_FAILED_TEXT'),
                'message' => $errorString
            ], 400);
        }

        $userCart->status = 'CHECKOUT';
        
        try {
            $userCart->save();

            return response()->json([
                'status' => env('STATUS_SUCCESS_TEXT'),
                'message' => 'Successfully checkout'
            ], 200);

        } catch (\Throwable $th) {
            return response()->json([
                'status' => env('STATUS_FAILED_TEXT'),
                'message' => 'Error While Checkout'
            ], 400);
        }
    }

    public function show($id)
    {
        $userCart = UserCart::where('user_id', $id)
        ->whereNull('status')
        ->orderBy('updated_at', 'DESC')
        ->first();

        if (!$userCart) {
            return response()->json([
                'status' => env('STATUS_FAILED_TEXT'),
                'message' => 'Data not found'
            ], 404);
        }

        try {
            $product = Product::getProductDetail($userCart->product_id);
        
            if ($product->status == 'success') {
                
                $cartDetail['id'] = $product->data->product->id ?? null;
                $cartDetail['title'] = $product->data->product->title ?? null;
                $cartDetail['picture_tumb'] = $product->data->product->picture_tumb ?? null;
                $cartDetail['price'] = $product->data->product->price ?? null;
                $cartDetail['qty'] = $userCart->qty ?? null;

                return response()->json([
                    'status' => env('STATUS_SUCCESS_TEXT'),
                    'data' => ([
                        'products' => $cartDetail])
                ], 200);
            }
            else{
                return response()->json([
                    'status' => env('STATUS_FAILED_TEXT'),
                    'message' => 'Product Data Not Found'
                ], 400);
            }
        } catch (\Throwable $th) {
            return response()->json([
                'status' => env('STATUS_FAILED_TEXT'),
                'message' => 'Error while connecting with Product API'
            ], 400);
        }
    }

    public function update(Request $request, $id)
    {
        $userCart = UserCart::find($id);
        
        if (!$userCart) {
            return response()->json([
                'status' => env('STATUS_FAILED_TEXT'),
                'message' => 'Data not found'
            ], 404);
        }
        
        $validator = Validator::make($request->all(), [
            "product_id" => "required"
        ]);
        
        if ($validator->fails()) {
            $errorString = implode(",",$validator->messages()->all());
            return response()->json([
                'status' => env('STATUS_FAILED_TEXT'),
                'message' => $errorString
            ], 400);
        }

        $data = $request->all();
        $userCart->fill($data);
        
        try {
            $userCart->save();

            return response()->json([
                'status' => env('STATUS_SUCCESS_TEXT'),
                'message' => 'Successfully updated cart'
            ], 200);

        } catch (\Throwable $th) {
            return response()->json([
                'status' => env('STATUS_FAILED_TEXT'),
                'message' => 'Error While Updating Data'
            ], 400);
        }
    }

    public function destroy($id)
    {
        $userCart = UserCart::find($id);
        
        if (!$userCart) {
            return response()->json([
                'status' => env('STATUS_FAILED_TEXT'),
                'message' => 'Data not found'
            ], 404);
        }

        try {
            $userCart->delete();
            return response()->json([
                'status' =>  env('STATUS_SUCCESS_TEXT'),
                'message' => 'Successfully Deleting Cart'
            ], 200);
        } catch (\Throwable $th) {
            return response()->json([
                'status' => env('STATUS_FAILED_TEXT'),
                'message' => 'Error While Deleting Cart'
            ], 400);
        }
    }
}