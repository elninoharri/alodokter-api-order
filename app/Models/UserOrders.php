<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UserOrders extends Model
{
    protected $table = 'user_orders';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id', 'product_id', 'status'
    ];

} 